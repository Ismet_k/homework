package prime.number;

import java.util.Scanner;

/**
 *
 * @author Ismet
 */
public class PrimeNumber {

   
    public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      boolean p = true;
         int num = sc.nextInt();
        for(int i = 2; i <= num/2; i++){
         
            if(num%i == 0){
                p = false;
                break;
            }
        }
     System.out.println(num+(p ? " is prime " : " is not prime"));
    }
}  
